require "test_helper"

class CreateArticlesTest < ActionDispatch::IntegrationTest

  setup do
    @user = User.create(username: "John", email: "john@example.com", password: "password")
  end

  test "get new article form and create article" do
    sign_in_as(@user, "password")
    get new_article_path
    assert_template "articles/new"
    assert_difference "Article.count", 1 do
      post articles_path, params: { article: { title:  "Article Title", description: "This is the body of the article and it must be 10 characters long.", user_id: 1 } }
      follow_redirect!
    end
    assert_template "articles/show"
    assert_equal "Article created!", flash[:success]
  end

  test "invalid article submission results in failure" do # tests creation failure
    sign_in_as(@user, "password")
    get new_article_path
    assert_template 'articles/new'
    assert_no_difference "Article.count" do
      post articles_path, params: { article: { title:  "This is a title", description: "xx" } }
    end
    assert_template 'articles/new'
    assert_select "h3.card-title"
    assert_match "1 error", response.body
    assert_match "prohibited this article from being saved", response.body
    assert_match "Description is too short (minimum is 10 characters)", response.body
  end

end
