require "test_helper"

class UserSignupTest < ActionDispatch::IntegrationTest

  test "get new user form and create user" do
    get signup_path
    assert_difference "User.count", 1 do
      post users_path, params: { user: { username:  "John", email: "john@example.com", password: "password" } }
      follow_redirect!
    end
    assert_template 'users/show'
    assert_equal "Welcome to the das412 Alpha Blog, John!", flash[:success]
    assert_match "articles", response.body
  end

  test "invalid new user submission results in failure" do
    get signup_path
    assert_select "form"
    assert_no_difference "User.count", 1 do
      post users_path, params: { user: { username:  "John", email: "john@example", password: "password" } }
    end
    assert_template 'users/new'
    assert_select "h3.card-title"
    assert_match "1 error", response.body
    assert_match "prohibited this article from being saved", response.body
    assert_match "Email is invalid", response.body
  end

end
